#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
chandra.aldiwijaya.694@gmail.com

Created on Tue Sep  8 17:10:20 2020

@author: bokab
"""


class program:
    dataFrame = {}
    
    def __init__(self, file):
        import pandas as pd
        
        self.dataFrame = pd.read_csv(file)
        
    def analyzeSVM(self):
        from sklearn import svm
        from sklearn.model_selection import train_test_split
        from sklearn.metrics import classification_report, confusion_matrix
        
        dataset = self.dataFrame.iloc [:,0:8]
        label = self.dataFrame.iloc [:,8]
        X_train, X_test, y_train, y_test = train_test_split(dataset, 
                                                            label, 
                                                            test_size=0.25,
                                                            random_state=150)
        model = svm.SVC(kernel='linear')
        model.fit(X_train, y_train)
        y_pred = model.predict(X_test)
        
        
        print("Confusion matrix untuk metode SVM")
        print(confusion_matrix(y_test, y_pred))
        print("\xa0")
        print("Classification report untuk metode KNN")
        print(classification_report(y_test, y_pred))
        #print("\n")
    
    def analyzeKNN(self, k=1):
        from sklearn.neighbors import KNeighborsClassifier
        from sklearn.model_selection import train_test_split
        from sklearn.metrics import classification_report, confusion_matrix
        
        dataset = self.dataFrame.iloc [:,2:4]
        label = self.dataFrame.iloc [:,8]
        X_train, X_test, y_train, y_test = train_test_split(dataset, 
                                                            label, 
                                                            test_size=0.25,
                                                            random_state=150)
        
        model = KNeighborsClassifier(n_neighbors=k)
        model.fit(X_train, y_train)
        y_pred = model.predict(X_test)
        
        print("Confusion matrix untuk metode KNN dengan k = " + str(k))
        print(confusion_matrix(y_test, y_pred))
        print("\xa0")
        print("Classification report untuk metode KNN dengan k = " + str(k))
        print(classification_report(y_test, y_pred))
        #print("\n")
        
    def searchK(self):
        import numpy as np
        from matplotlib import pyplot as plt
        from sklearn.neighbors import KNeighborsClassifier
        from sklearn.model_selection import train_test_split
        
        dataset = self.dataFrame.iloc [:,2:4]
        label = self.dataFrame.iloc [:,8]
        X_train, X_test, y_train, y_test = train_test_split(dataset, 
                                                            label, 
                                                            test_size=0.25,
                                                            random_state=150)
        
        error = []
        for i in range(1, 50):
            model = KNeighborsClassifier(n_neighbors=i)
            model.fit(X_train, y_train)
            y_pred = model.predict(X_test)
            error.append(np.mean(y_pred != y_test))
        
        fig, ax = plt.subplots(figsize=(8,8))
        ax.plot(range(1, 50), error, color='red', marker='o', markersize=5)
        ax.set_title('Error pada nilai K model KNN')  
        ax.set_xlabel('K')  
        ax.set_ylabel('Error rata-rata')
        plt.show()
        
        index_max = np.argmin(error)
        bestK = range(1, 50)[index_max]
        print("Berdasarkan plot, kita mendapatkan bahwa:")
        print("Nilai K terbaik = " + str(bestK))
        print("Dengan error minimum = " + str(min(error)))
        print("\xa0")
        
        try:
            return bestK
        
        except:
            pass
        
    
    
def start():
    csvFile = "pulsar_stars.csv"
    
    run = program(csvFile)
    run.analyzeSVM()
    run.analyzeKNN()
    k = run.searchK()
    run.analyzeKNN(k) 

if __name__ == "__main__":
    start()