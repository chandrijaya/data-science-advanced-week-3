    #!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
chandra.aldiwijaya.694@gmail.com

Created on Thu Sep 10 17:08:05 2020

@author: bokab
"""


import tweepy


def start():
    consumer_key = ""
    consumer_secret = ""
    access_token = ""
    access_token_secret = ""
    
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)
    api = tweepy.API(auth)
    
    userName = "jokowi" #akun yang diperiksa
    totalTweet = 200 #Input jumlah tweet yang diambil
    
    sampleTweet = api.user_timeline(id=userName, count=totalTweet)
    
    tweetCount = 0 #Hitung tweet yang ditemukan keyword
    keyWord = "Covid" #Input keyword yang dicari per-tweet
    for tweet in sampleTweet:
        if keyWord.casefold() in tweet.text.casefold(): #Tidak case sensitive
            tweetCount += 1
    
    
    print("Total sample tweet dari akun @" + str(userName) + 
          " adalah: " + str(totalTweet))
    print("Total tweet dengan topik '" + str(keyWord) + 
          "' dari akun @" + str(userName) + " adalah: " + str(tweetCount))

if __name__ == "__main__":
    start()