#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 12 19:20:07 2020

@author: bokab
"""




#import pandas as pd
#import matplotlib.pyplot as plt
#import numpy as np


def start():
    import tweepy
    import sqlite3
    
    consumerKey = ""
    consumerSecret = ""
    accessToken = ""
    accessTokenSecret = ""
    
    auth = tweepy.OAuthHandler(consumerKey, consumerSecret)
    auth.set_access_token(accessToken, accessTokenSecret)
    api = tweepy.API(auth, wait_on_rate_limit=True)
    
    searchWords = "PSBB"
    newSearch = searchWords + " -filter:retweets"
    tweetCount = 500

    tweets = tweepy.Cursor(api.search,
            q=newSearch,
            lang="id").items(tweetCount)
    
    db_file = "chandra.aldiwijaya.694.db"
    connection = sqlite3.connect(db_file)
    cursor = connection.cursor()
    
    create_tweet_samples_table = '''CREATE TABLE IF NOT EXISTS tweet_samples (
                                 id INTEGER PRIMARY KEY AUTOINCREMENT,
                                 tweet_id TEXT NOT NULL,
                                 tweet_text integer NOT NULL);'''
    
    crud_query = '''insert into tweet_samples (tweet_id, tweet_text) 
                    values (?,?);'''
    
    cursor.execute(create_tweet_samples_table)
    for tweet in tweets:
        data = (tweet.id, tweet.text)
        cursor.execute(crud_query,data)
    
    connection.commit()
    cursor.close()
    connection.close()
    
def start2():
    import re
    import sqlite3
    import pandas as pd
    import numpy as np
    import matplotlib.pyplot as plt
    
    db_file = "chandra.aldiwijaya.694.db"
    connection = sqlite3.connect(db_file)
    cursor = connection.cursor()
    
    crud_query ='''SELECT * FROM tweet_samples'''
    
    cursor = connection.cursor()
    cursor.execute(crud_query)
    get_sql =  cursor.fetchall()
    cursor.close()
    connection.close()
    
    header = ["id", "tweet_id", "tweet_text"]
    df = pd.DataFrame(get_sql, columns=header)
    
    posList= open("kata_positif.txt","r")
    posWord = posList.readlines()
    negList= open("kata_negatif.txt","r")
    negWord = negList.readlines()
    
    temp = []
    for index, row in df.iterrows():
        countP = 0
        countN = 0
        text = ' '.join(re.sub("(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])|(\w+:\/\/\S+)", " ", row.tweet_text).split())
        text = text.casefold()
        for pos in posWord:
            if pos.strip() in text:
                countP +=1
                
        for neg in negWord:
            if neg.strip() in text:
                countN +=1
                    
        temp.append(countP - countN)
        df.at[index, "tweet_text"] = text
    
    df["value"] = temp
    print("Mean: "+str(np.mean(df["value"])))
    print("Median: "+str(np.median(df["value"])))
    print("Standard Deviation: "+str(np.std(df["value"])))
    
    labels, counts = np.unique(df["value"], return_counts=True)
        
    plt.figure()
    plt.bar(labels, counts, align='center')
    plt.gca().set_xticks(labels)
        
    plt.show()
    
if __name__ == "__main__":
    start()
    start2()