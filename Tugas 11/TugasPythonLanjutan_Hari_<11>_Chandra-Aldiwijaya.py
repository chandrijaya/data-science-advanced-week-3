#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
chandra.aldiwijaya.694@gmail.com

Created on Mon Sep  7 20:33:08 2020

@author: bokab
"""


class program:
    from sklearn.cluster import KMeans
    import numpy as np
    import matplotlib.pyplot as plt
    
    csvFile = ""; url = ""; dataFrame = {}; dataSample = []
    
    def __init__(self, fileName, url):
        self.csvFile = fileName
        self.url = url
    
    def genCsv(self, maxIndex=10, tableHead=[], multiValueCol=[], 
               countMultiValue=[], spliter=',', removeFirstCol=False):
        
        import requests, csv
        from bs4 import BeautifulSoup
        
        req = requests.get(self.url)
        html = BeautifulSoup(req.content, 'html5lib')
        tableList = html.find("table", {"class":"data-table"})
        tableBody = tableList("tr")
        tableBody.pop(0)
        
        f = csv.writer(open(self.csvFile, "w"))
        if tableHead:
            f.writerow(tableHead)
            
        for noRow, row in enumerate(tableBody):
            temp = []
            try:
                if int(row.contents[3].get_text()) > maxIndex:
                    break
                
            except:
                if noRow == maxIndex:
                    break
            
            if removeFirstCol:
                temp.append(noRow+1)
                
            for noCell, cell in enumerate(row(["td", "th"])):
                value = (cell.get_text()).replace('\n','')
                if (noCell in multiValueCol):
                    if not countMultiValue:
                        from os import remove
                        
                        remove(self.csvFile)
                        print("Error! Make sure you input integer countMultiValue")
                        return ValueError("Fatal Error! Can't generate a csv file!")
                        
                    value = value.split(spliter[multiValueCol.index(noCell)])
                    multi = countMultiValue[multiValueCol.index(noCell)]
                    for i in range(multi):
                        try:
                            temp.append(value[i])
                        
                        except:
                            temp.append(None)
                    
                    
                else:
                    try:
                        temp.append(int(value))
                    
                    except:
                        try:
                            temp.append(float(value))
                    
                        except:
                            temp.append(value)
                            
                            
            if removeFirstCol:        
                temp.pop(1)
                
            f.writerow(temp)
        
                
    def initDF(self, X, Y, method=False):
        import pandas as pd
        
        self.dataFrame = pd.read_csv(self.csvFile)
        if (method):
            methods = ["log", "normalization"]
            if method not in methods:
                print("Error! No such a method you choose")
                print("Fatal Error! Can't generate data sample")
                return
                
            i = len(self.dataFrame.columns)
            
            t = methods.index(method)
            if (t == 0):
                self.dataFrame["Xtrans"] = self.np.log(self.dataFrame[X])
                self.dataFrame["Ytrans"] = self.np.log(self.dataFrame[Y])
                
            elif (t == 1):
                self.dataFrame["Xtrans"] = ((self.dataFrame[X] - self.dataFrame[X].min())/
                              (self.dataFrame[X].max() - self.dataFrame[X].min()))
                self.dataFrame["Ytrans"] = ((self.dataFrame[Y] - self.dataFrame[Y].min())/
                              (self.dataFrame[Y].max() - self.dataFrame[Y].min()))
            
            else:
                print("Error! No such a method you choose")
                print("Fatal Error! Can't generate data sample")
                return
            
            self.dataSample = self.np.array(self.dataFrame.iloc[:, i:i+2])
            
        else:
            self.dataSample = self.np.array(self.dataFrame[[X, Y]])
        
    
    def analyzeKMeans(self, X, Y, K):
        from os import path
        
        try:
            kmeans = self.KMeans(n_clusters=K, random_state=500)
            kmeans.fit(self.dataSample)
            self.dataFrame['Cluster'] = kmeans.labels_
        except:
            print("Error! Make sure you have run initDF()")
            return
        
        fig, ax = self.plt.subplots(figsize=(8,8))
        ax.scatter(self.dataFrame[X], self.dataFrame[Y], 
                    s=20, c=self.dataFrame.Cluster, marker="o", alpha=0.5)
        ax.set_title("Hasil Clustering K-Means")
        ax.set_xlabel(X)
        ax.set_ylabel(Y)
        self.plt.show()
        
        newFile = ("[New]_" + self.csvFile)
        if path.isfile(newFile):
            from os import remove
                        
            remove(newFile)
            
        self.dataFrame.to_csv(newFile, index=False, encoding='utf-8')
    
    def analyzeElbow(self):
        sse = []
        k_list = []
        for k in range(1, 11):
            kmeans = self.KMeans(n_clusters = k, 
                                 random_state=500).fit(self.dataSample)
            centroids = kmeans.cluster_centers_
            prediksi = kmeans.predict(self.dataSample)
            nilai_sse = 0
            
            for i in range(len(self.dataSample)):
                titik_pusat = centroids[prediksi[i]]
                nilai_sse += (((self.dataSample[i, 0] - titik_pusat[0]) ** 2) + 
                              ((self.dataSample[i, 1] - titik_pusat[1]) ** 2))
            
            sse.append(nilai_sse)
            k_list.append(k)
        
        fig, ax = self.plt.subplots(figsize=(8,8))
        ax.plot(k_list, sse)
        self.plt.show()
        
    def analyzeSillhoute(self):
        from sklearn.metrics import silhouette_score

        data = []
        k_list = []
        try:
            for k in range(2, 11):
                kmeans = self.KMeans(n_clusters = k, 
                                     random_state=500).fit(self.dataSample)
                labels = kmeans.labels_
                data.append(silhouette_score(self.dataSample, 
                                             labels, metric = 'euclidean'))
                k_list.append(k)
            
        
        except:
            print("Error! Make sure you have run initDF()")
            return -1
        
        fig, ax = self.plt.subplots(figsize=(8,8))
        ax.plot(k_list,data)
        self.plt.show()
        
        index_max = self.np.argmax(data)
        print("Berdasarkan plot, kita mendapatkan bahwa:")
        print("Nilai K terbaik = " + str(k_list[index_max]))
        print("Dengan Sillhoute Score = " + str(max(data)))
        
        try:
            return k_list[index_max]
        
        except:
            pass
        

def start():
    from os import path
    
    #<---Parameter yang dapat diubah oleh user--->
    fileName = "chandra.aldiwijaya.694.csv"
    url = "https://pokemondb.net/pokedex/all"
    #Jangan lupa hapus csv file (jika ada) jika parameter dibawah ada yang diubah
    MaxIndex = 500 #Penentuan batas index pengambilan data pada tabel
    TableHead = ["No", "Name", "Type 1", "Type 2", "Total", "HP", 
                 "Attack", "Defense", "Sp. Atk", "Sp. Def", "Speed"] #Custom header tabel
    MultiValueCol = [2] #Kolom-kolom yang berpotensi berisi dua value atau lebih, jika ada. Harus list!
    CountMultiValue = [2] #Jumlah value pada masing-masing kolom berpotensi dua value atau lebih, jika ada. Harus list!
    Spliter = [' '] #Pemisah pada masing-masing kolom multi value. Harus list!
    RemoveFirstCol = False #Jika ingin menghapus dan mengganti kolom penomoran secara default
    #Pemilihan sample data
    x = "Attack" #X_train, berdasarkan TableHead
    y = "Defense" #y_train, berdasarkan TableHead
    k = 4 #Karena menggunakan model KMeans, ditentukan oleh nilai k
    #</---Parameter yang dapat diubah oleh user--->
    
    run = program(fileName, url)
    if not path.isfile(fileName):
        run.genCsv(MaxIndex, TableHead, MultiValueCol, 
                   CountMultiValue, Spliter, RemoveFirstCol)
        
    
    if path.isfile(fileName):
        run.initDF(x, y, method="normalization") #Wajib dijalankan di awal sebelum analisis. method = ["log", "normalization"]
        run.analyzeKMeans(x, y, k)
        run.analyzeElbow()
        kNew = run.analyzeSillhoute() #return -1 jika gagal analisis
        if (kNew != k and kNew != -1):
            run.analyzeKMeans(x, y, kNew)


if __name__ == "__main__":
    start()

