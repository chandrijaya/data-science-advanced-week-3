#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
chandra.aldiwijaya.694@gmail.com

Created on Sat Sep 12 13:45:10 2020

@author: bokab
"""


import tweepy
import re
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np


def start():
    consumerKey = ""
    consumerSecret = ""
    accessToken = ""
    accessTokenSecret = ""
    
    auth = tweepy.OAuthHandler(consumerKey, consumerSecret)
    auth.set_access_token(accessToken, accessTokenSecret)
    api = tweepy.API(auth, wait_on_rate_limit=True)
    
    '''
    indonesiaWoe = 23424846
    
    trends = api.trends_place(indonesiaWoe)
    
    hashtagedTrend = []
    simpleWordTrend = []
    for item in (trends[0]['trends']):
        if ("#" in item['name']):
            hashtagedTrend.append(item['name'])
            
        else:
            simpleWordTrend.append(item['name'])
            
    '''
    searchWords = ["#DeklarasiPilkadaPatuhProkes", "Anies"]
    for no, word in enumerate(searchWords):
        dateSince = "2020-09-01"
        newSearch = word + " -filter:retweets"
        tweetCount = 100
        title = ("Analisis sentimen trend '" + str(word) + "' pada " + str(tweetCount) + 
                 " sampel tweet sejak " + str(dateSince) + " di Indonesia")
        tweets = tweepy.Cursor(api.search,
                q=newSearch,
                lang="id",
                since=dateSince).items(tweetCount)
        
        
        
        items = []
        for tweet in tweets:
            items.append (' '.join(re.sub("(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])|(\w+:\/\/\S+)", " ", tweet.text).split()))
        
        df = pd.DataFrame(data=items, columns=['tweet'])
        
        posList= open("./kata_positif.txt","r")
        posWord = posList.readlines()
        negList= open("./kata_negatif.txt","r")
        negWord = negList.readlines()
        
        temp = []
        for item in items:
            count_p = 0
            count_n = 0
            for pos in posWord:
                if pos.strip() in item:
                    count_p +=1
                    
            for neg in negWord:
                if neg.strip() in item:
                    count_n +=1
                    
            #print ("positif: "+str(count_p))
            #print ("negatif: "+str(count_n))
            temp.append(count_p - count_n)
            # print ("-----------------------------------------------------")
        
        df["value"] = temp
        print(title)
        print ("Nilai rata-rata: "+str(np.mean(df["value"])))
        print ("Standar deviasi: "+str(np.std(df["value"])))
        if (np.mean(df["value"]) < 0):
            print("Hasil analisis sentimen menunjukan bahwa sentimen netizen tweeter terhadap trend '" + 
                  str(word) + "'memiliki kecendrungan respon NEGATIF")
            
        else:
            print("Hasil analisis sentimen menunjukan bahwa sentimen netizen tweeter terhadap trend '" + 
                  str(word) + "'memiliki kecendrungan respon POSITIF")
        
        print("\x0a")
        labels, counts = np.unique(df["value"], return_counts=True)
        
        plt.figure((no+1))
        plt.bar(labels, counts, align='center')
        plt.title(title)
        plt.gca().set_xticks(labels)
        
    plt.show()
    
if __name__ == "__main__":
    start()