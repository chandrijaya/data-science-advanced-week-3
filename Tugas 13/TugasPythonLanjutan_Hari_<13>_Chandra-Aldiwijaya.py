#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
chandra.aldiwijaya.694@gamil.com

Created on Thu Sep 10 14:02:44 2020

@author: bokab
"""

import pandas as pd
import matplotlib.pyplot as plt


def start():
    indonesia = pd.read_json("indonesia-confirmed-date.json")
    south_korea = pd.read_json("korea-south-confirmed-date.json")
    vietnam = pd.read_json("vietnam-confirmed-date.json")
    months = ["Jul", "Aug", "Sept"]
    labels = []
    ticks = []
    count = 1
    for month in months:
        for i in range(1,32):
            if (i == 2) and (month == "Sept"):
                break
            
            if ((count%7) == 0):
                date = (str(i) + " " + str(month))
                labels.append(date)
                ticks.append(count)
            
            count += 1
            
    x = range(1, len(indonesia)+1)
    
    fig, ax = plt.subplots(figsize=(10,7))
    ax.plot(x, indonesia.Cases, label="Indonesia")
    ax.plot(x, south_korea.Cases, label="Korea Selatan")
    ax.plot(x, vietnam.Cases, label="Vietnam")
    ax.set_title('Kasus Covid19 terkonfirmasi 1 Agustus 2020 - 1 September 2020')  
    ax.set_xlabel('Tanggal')  
    ax.set_ylabel('Terkonfirmasi')
    ax.set_xticks(ticks)
    ax.set_xticklabels(labels, rotation=40, ha="center")
    ax.legend(loc=("upper left"))
    plt.show()
    
    print("Terhitung Vietnam belum mendapatkan kasus terkonfirmasi Covid19 sampai 1 September 2020.")
    print("Korea Selatan mengalami sedikit pertambahan kasus setelah tanggal 11 Agustus 2020.")
    print("Indonesia masih mengalami trend peningkatan tinggi kasus terkonfirmasi Covid19 dibandingkan dengan 2 negara lain yang terplotkan.")

if __name__ == "__main__":
    start()